import React, { Component } from 'react';
import './Product.css';
import { Table, Header } from 'semantic-ui-react';

class Product extends Component {
  render() {
    return (
      <>
        <Header as='h3'>{this.props.name}</Header>
        <Table celled definition>
          <Table.Body>
            <Table.Row>
              <Table.Cell>ASIN</Table.Cell>
              <Table.Cell>{this.props.id}</Table.Cell>
            </Table.Row>
            <Table.Row>
              <Table.Cell>Name</Table.Cell>
              <Table.Cell>{this.props.name}</Table.Cell>
            </Table.Row>
            <Table.Row>
              <Table.Cell>Category</Table.Cell>
              <Table.Cell>{this.props.category}</Table.Cell>
            </Table.Row>
            <Table.Row>
              <Table.Cell>Rank</Table.Cell>
              <Table.Cell>{this.props.rank && <>#{this.props.rank} in {this.props.category}</>}</Table.Cell>
            </Table.Row>
            <Table.Row>
              <Table.Cell>Dimensions</Table.Cell>
              <Table.Cell>{this.props.dimensions}</Table.Cell>
            </Table.Row>
          </Table.Body>
        </Table>
      </>
    );
  }
}

export default Product;

import React, { Component } from 'react';
import './App.css';
import Product from './Product';
import { Form, Grid, Header, Container, Segment } from 'semantic-ui-react';

class App extends Component {
  constructor(props) {
    super(props);
    this.state = {
      product: null,
      productId: '',
      loading: false,
      error: ''
    };

    this.onChange = this.onChange.bind(this);
    this.onSubmit = this.onSubmit.bind(this);
  }

  getProduct = async (id) => {
    const response = await fetch(`/products/${id}`);
    if (response.status === 200) {
      const body = await response.json();
      return body;
    }
    const text = await response.text();
    throw Error(text);
  }

  onSubmit(event) {
    event.preventDefault();
    if (this.state.product && this.state.product.id === this.state.productId) {
      return;
    }
    this.setState({ ...this.state, loading: true, error: '' });
    this.getProduct(this.state.productId)
      .then(product => this.setState({
        ...this.state,
        product: product,
        loading: false
      }))
      .catch(err => this.setState({ ...this.state, product: null, loading: false, error: err.message }));
  }

  onChange(event) {
    this.setState({
      ...this.state,
      productId: event.target.value
    });
  }

  render() {
    return (
      <Container>
        <Grid columns={1} centered>
          <Grid.Column width={10}>
            <Header as='h1' textAlign='center' style={{ margin: '2rem 0 4rem 0' }}>Amazon Product Info</Header>
            <Form onSubmit={this.onSubmit} style={{ margin: '4rem 0' }}>
              <Form.Group>
                <Form.Input placeholder='ASIN' type='text' value={this.state.productId} onChange={this.onChange} loading={this.state.loading} icon='search' />
                <Form.Button type='submit' primary>Find</Form.Button>
              </Form.Group>
            </Form>
            {this.state.error &&
              <Segment color='yellow'>{this.state.error}</Segment>
            }
            {this.state.product &&
              <Product id={this.state.product.id}
                name={this.state.product.name}
                rank={this.state.product.rank}
                dimensions={this.state.product.dimensions}
                category={this.state.product.category}>
              </Product>
            }
          </Grid.Column>
        </Grid>
      </Container>
    );
  }
}

export default App;

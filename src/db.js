const mongoose = require('mongoose');
const config = require('config');

const schema = new mongoose.Schema({
  id: 'string',
  name: 'string',
  dimensions: 'string',
  rank: 'number',
  category: 'string'
});

const Product = mongoose.model('Product', schema);

const connect = async () => {
  try {
    await mongoose.connect(config.get('mongodbUri'));
  } catch (e) {
    throw new Error('Unable to connect to database');
  }
}

const disconnect = () => {
  mongoose.disconnect();
}

const getProduct = async (id) => {
  const product = await Product.findOne({ id });
  if (!product) {
    return null;
  }
  return {
    id: product.id,
    name: product.name,
    dimensions: product.dimensions,
    rank: product.rank,
    category: product.category
  };
}

const saveProduct = async (product) => {
  const entry = new Product(product);
  await entry.save();
}

module.exports = {
  connect,
  disconnect,
  getProduct,
  saveProduct
};

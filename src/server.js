const fastify = require('fastify');
const handlers = require('./handlers');

const createServer = async () => {
  const server = fastify({ logger: true });
  server.get('/products/:id', handlers.getProduct);
  await server.listen(5000);
  return server;
}

module.exports = {
  createServer
}

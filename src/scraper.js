const puppeteer = require('puppeteer');

const CATEGORY_REGEX = /#([\d,]+) in (.+)?\(/;
const SINGLE_CATEGORY_REGEX = /#([\d,]+) in (.+)?/; // this is a bit lazy

const getProductUrl = (id) => {
  return `https://www.amazon.com/dp/${id}`;
}

const getProduct = async (id) => {
  const browser = await puppeteer.launch();
  const page = await browser.newPage();
  const response = await page.goto(getProductUrl(id));

  if (!response.ok()) {
    return null;
  }

  // we might not be able to scrape everything but do our best
  const product = {
    id,
    name: '',
    dimensions: '',
    rank: '',
    category: ''
  };

  try {
    product.name = await page.$eval('#productTitle', el => el.textContent.trim());
  } catch (e) {
    // pretend all is good in the world
  }

  // attempt to get the dimensions
  let dimensions = await page.$$eval('tr.size-weight', (rows) => {
    for (row of rows) {
      if (row.querySelector('td.label').textContent.includes('Dimensions')) {
        return row.querySelector('td.value').textContent.trim();
      }
    }
  });

  // try another one
  if (!dimensions) {
    dimensions = await page.$$eval('#detailBullets span.a-list-item', (rows) => {
      for (row of rows) {
        if (row.querySelector('span.a-text-bold').textContent.includes('Dimensions')) {
          return row.querySelector('span:last-child').textContent.trim();
        }
      }
    });
  }

  // and another one
  if (!dimensions) {
    dimensions = await page.$$eval('table.prodDetTable tr', (rows) => {
      for (row of rows) {
        if (row.querySelector('th').textContent.includes('Dimensions')) {
          return row.querySelector('td').textContent.trim();
        }
      }
    });
  }

  // give up by this point
  product.dimensions = dimensions || '';

  let rank;
  try {
    rank = await page.$eval('#SalesRank', el => el.textContent);
  } catch (e) {
    // do nothing
  }

  // lets try this again
  if (!rank) {
    rank = await page.$$eval('table.prodDetTable tr', (rows) => {
      for (row of rows) {
        if (row.querySelector('th').textContent.includes('Rank')) {
          return row.querySelector('td').textContent.trim();
        }
      }
    });
  }

  if (rank) {
    let matches = rank.match(CATEGORY_REGEX);
    // sometimes it's very basic
    if (!matches) {
      matches = rank.match(SINGLE_CATEGORY_REGEX);
    }
    if (matches) {
      product.rank = parseInt(matches[1].replace(/,/g, ''));
      product.category = matches[2].trim();
    }
  }

  browser.close();
  return product;
}

module.exports = { getProduct };

const db = require('./db');
const server = require('./server');

const start = async () => {
  // open connection to database
  try {
    await db.connect();
  } catch (e) {
    console.error(e);
    return;
  }

  // run server
  try {
    const listener = await server.createServer();
    return listener;
  } catch (e) {
    db.disconnect();
    console.error(e);
  }
}

module.exports = { start };

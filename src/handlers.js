const db = require('./db');
const scraper = require('./scraper');

const getProductHandler = async (req, res) => {
  // try to retrieve it from db
  let product;
  try {
    product = await db.getProduct(req.params.id);
  } catch (e) {
    req.log.error(e);
    // we can still scrape it
  }

  if (!product) {
    // go scrape it
    try {
      product = await scraper.getProduct(req.params.id);
      if (!product) {
        return res.status(404).send('Product does not exist')
      }
    } catch (e) {
      return res.status(500).send('Error retrieving product info from Amazon');
    }

    // store product
    try {
      await db.saveProduct(product);
    } catch (e) {
      // if it fails to save we'll just have to keep scraping
      req.log.error(e);
    }
  }

  return product;
};

module.exports = { getProduct: getProductHandler };

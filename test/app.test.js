const mocha = require('mocha');
const request = require('supertest');
const sinon = require('sinon');
const db = require('../src/db');
const server = require('../src/server');
const sinonChai = require('sinon-chai');
const chai = require('chai');
const scraper = require('../src/scraper');
const startApp = require('../src/app').start;

chai.use(sinonChai);
const expect = chai.expect;

describe('server', () => {
  const product = {
    id: 'B01GW8YDLK',
    name: 'Marvel’s Spider-Man - PlayStation 4',
    dimensions: '0.6 x 5.3 x 6.7 inches; 3.2 ounces',
    rank: '20',
    category: 'Video Games'
  };

  let dbConnectStub;
  let dbDisconnectStub;
  let dbGetStub;
  let scrapeStub;
  let createServerStub;

  beforeEach(async () => {
    dbConnectStub = sinon.stub(db, 'connect').returns(Promise.resolve());
    dbDisconnectStub = sinon.stub(db, 'disconnect').returns(Promise.resolve());
    dbGetStub = sinon.stub(db, 'getProduct');
    dbSaveStub = sinon.stub(db, 'saveProduct');
    scrapeStub = sinon.stub(scraper, 'getProduct').returns(Promise.resolve(product));
    createServerStub = sinon.stub(server, 'createServer').callThrough();
  });

  afterEach(async () => {
    dbConnectStub.restore();
    dbDisconnectStub.restore();
    dbGetStub.restore();
    dbSaveStub.restore();
    scrapeStub.restore();
    createServerStub.restore();
  });

  it('should stop if theres an error connecting to the database', async () => {
    dbConnectStub.throws(new Error('Could not connect'));
    const app = await startApp();
    expect(app).to.be.undefined;
    expect(createServerStub).to.not.have.been.called;
  });

  it('should close the database connection if the server fails to start', async () => {
    createServerStub.throws(new Error('Failed to start'));
    const app = await startApp();
    expect(dbDisconnectStub).to.have.been.called;
  });

  it('should retrieve product from database', async () => {
    dbGetStub.returns(Promise.resolve(product));
    const app = await startApp();

    const response = await request(app.server).get('/products/B01GW8YDLK');
    expect(response.body).to.eql(product);
    expect(response.status).to.equal(200);
    expect(dbGetStub).to.have.been.calledOnceWith('B01GW8YDLK');
    expect(scrapeStub).not.to.have.been.called;
    expect(dbSaveStub).not.to.have.been.called;

    await app.close();
  });

  it('should scrape product if it does not exist in database', async () => {
    dbGetStub.returns(Promise.resolve(null));
    const app = await startApp();

    const response = await request(app.server).get('/products/B01GW8YDLK');
    expect(response.body).to.eql(product);
    expect(response.status).to.equal(200);
    expect(dbGetStub).to.have.been.calledOnceWith('B01GW8YDLK');
    expect(scrapeStub).to.have.been.calledOnceWith('B01GW8YDLK');
    expect(dbSaveStub).to.have.been.calledOnceWith(product);

    await app.close();
  });

  it('should scrape product if there was an error retrieving it from database', async () => {
    dbGetStub.throws(new Error('Oops'));
    const app = await startApp();

    const response = await request(app.server).get('/products/B01GW8YDLK');
    expect(response.body).to.eql(product);
    expect(response.status).to.equal(200);
    expect(dbGetStub).to.have.been.calledOnceWith('B01GW8YDLK');
    expect(scrapeStub).to.have.been.calledOnceWith('B01GW8YDLK');
    expect(dbSaveStub).to.have.been.calledOnceWith(product);

    await app.close();
  });

  it('should return 500 if there was an error scraping product', async () => {
    dbGetStub.returns(Promise.resolve(null));
    scrapeStub.throws(new Error('Oops'));
    const app = await startApp();

    const response = await request(app.server).get('/products/B01GW8YDLK');
    expect(response.text).to.equal('Error retrieving product info from Amazon');
    expect(response.status).to.equal(500);
    expect(dbGetStub).to.have.been.calledOnceWith('B01GW8YDLK');
    expect(scrapeStub).to.have.been.calledOnceWith('B01GW8YDLK');
    expect(dbSaveStub).not.to.have.been.called;

    await app.close();
  });

  it('should return 404 if no product found to scrape', async () => {
    dbGetStub.returns(Promise.resolve(null));
    scrapeStub.returns(Promise.resolve(null));
    const app = await startApp();

    const response = await request(app.server).get('/products/B01GW8YDLK');
    expect(response.text).to.equal('Product does not exist');
    expect(response.status).to.equal(404);
    expect(dbGetStub).to.have.been.calledOnceWith('B01GW8YDLK');
    expect(scrapeStub).to.have.been.calledOnceWith('B01GW8YDLK');
    expect(dbSaveStub).not.to.have.been.called;

    await app.close();
  });

  it('should still return product if it failed to save to the database', async () => {
    dbGetStub.returns(Promise.resolve(null));
    scrapeStub.returns(Promise.resolve(product));
    dbSaveStub.throws(new Error('Oops'));
    const app = await startApp();

    const response = await request(app.server).get('/products/B01GW8YDLK');
    expect(response.body).to.eql(product);
    expect(response.status).to.eql(200);
    expect(dbGetStub).to.have.been.calledOnceWith('B01GW8YDLK');
    expect(scrapeStub).to.have.been.calledOnceWith('B01GW8YDLK');
    expect(dbSaveStub).to.have.been.calledOnceWith(product);

    await app.close();
  });
});

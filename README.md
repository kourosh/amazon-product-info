# amazon-product-info

Given an ASIN will:
 - Scrape the product details from the Amazon product page
 - Store it in a MongoDB database
 - Display it to the user

 Any future requests for that product will load the product from the database.

 We will try to scrape what we can, if data is missing we'll render what we have.

 ## Requirements
 - Node.js
 - Yarn
 - MongoDB
 - Google Chrome

## Running
You will need a MongoDB instance running on `localhost:27017` (or update the config file).

Install all packages
```
yarn
```
Run the development server
```
yarn dev
```

 ## TODO
 - Make it deployable
 - Add UI tests
 - Improve test coverage
 - Make scraper more robust
 - Convert to TypeScript
 - Update stored product if it has changed or since removed
 - Separate scraper into its own app that can process a queue of products and notify clients when complete



